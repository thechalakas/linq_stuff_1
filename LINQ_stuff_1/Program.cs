﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQ_stuff_1
{
    class Program
    {
        static void Main(string[] args)
        {

            //running some basic linq stuff
            simple_linq_query_1();

            //running the saem basic linq stuff but differently using lambda expressions
            simple_linq_query_2();

            //doing linq with xml
            linq_with_xml_1();

            Console.WriteLine("SWEAR TO ME!!!!!");
            Console.WriteLine("That's all folks");
            Console.ReadLine();
            

        }

        //running linq queries with xml
        private static void linq_with_xml_1()
        {
            string xml_string = return_xml_1();
            XDocument document_xml_1 = XDocument.Parse(xml_string);

            //doing linq and getting a list of elements we want
            IEnumerable<string> list_of_person = from temp in document_xml_1.Descendants("person")
                                                 select (string)temp.Attribute("firstname") + " " + (string)temp.Attribute("lastname");

            //i got the list of people from the xml file
            //now let me display it like i would display any list
            Console.WriteLine("got a total of {0} persons", list_of_person.Count());
            foreach(string person in list_of_person)
            {
                Console.WriteLine("person found is {0}", person);
            }
        }

        //this method will do exactly what simple_linq_query_1 is doing
        //except it uses a different kind of linq query
        private static void simple_linq_query_2()
        {
            //first lets a simple data array
            int[] list_to_run_queries_1 = some_data_1();

            //contrast this with what you found in simple_linq_query_1
            //you will see that this one does the exact same thing in exactly one line of code.
            var result_int_list = list_to_run_queries_1.Where(t => t % 3 == 0);

            //now, I will simply display results
            Console.WriteLine("The results (from the different kind) of the LINQ query as they appear in the result list");
            foreach (int i in result_int_list)
            {
                Console.WriteLine(" the element is {0}", i);
            }

            //descending display
            result_int_list.OrderByDescending(t => t);

            Console.WriteLine("The results (from the different kind) of the LINQ query as they appear in the result list, descending display");
            foreach (int i in result_int_list)
            {
                Console.WriteLine(" the element is {0}", i);
            }
            //ascending display
            result_int_list.OrderBy(t => t);
            Console.WriteLine("The results (from the different kind) of the LINQ query as they appear in the result list, ascending display");
            foreach (int i in result_int_list)
            {
                Console.WriteLine(" the element is {0}", i);
            }
        }

        //this method will do a simple query
        private static void simple_linq_query_1()
        {
            //first lets a simple data array
            int[] list_to_run_queries_1 = some_data_1();

            //collecting all numbers from the list which are divisible by 3
            //the LINQ functionality will now go through the list, taking each item one at a time
            //then, divide it by 3 and then if the results match, add it a result list
            //the result list is then added to result_1 as an int array.
            var result_1 = from temp in list_to_run_queries_1
                           where temp % 3 == 0
                           select temp;

            //now, I will simply display results
            Console.WriteLine("The results of the LINQ query as they appear in the result list");
            foreach(int i in result_1)
            {
                Console.WriteLine(" the element is {0}", i);
            }

            //now, let me try sorting them in descending order
            Console.WriteLine("The results of the LINQ query as they appear in the result list, in descending order");
            result_1 = from temp in result_1
                       orderby temp descending
                       select temp;

            foreach (int i in result_1)
            {
                Console.WriteLine(" the element is {0}", i);
            }


            //now, let me try sorting them in ascending order
            Console.WriteLine("The results of the LINQ query as they appear in the result list, in ascending order");
            result_1 = from temp in result_1
                       orderby temp ascending
                       select temp;
            foreach (int i in result_1)
            {
                Console.WriteLine(" the element is {0}", i);
            }

        }


        //in this method, I am returning a simple xml file. 
        //since this whole solution is about playing around with xml files
        //this would be better
        static string return_xml_1()
        {
            string temp_xml = "";

            temp_xml = @"<?xml version=""1.0"" encoding=""utf-8""?>
                        <people>
                            <person firstname=""study"" lastname=""nildana"">
                                <contactdetails>
                                    <emailaddress>support@studynildana.com</emailaddress>
                                </contactdetails>
                            </person>
                            <person firstname=""study"" lastname=""nildana2"">
                                <contactdetails>
                                    <emailaddress>support@studynildana.com</emailaddress>
                                    <phonenumber>5551234567</phonenumber>
                                </contactdetails>
                            </person>    
                        </people>";

            return (temp_xml);
        }

        //returns a simple int array with some numbers for demonstrating usage of LINQ
        static int[] some_data_1()
        {
            //building a simple list with some numbers
            int[] to_return = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12 };

            //returnin the list
            return (to_return);
        }
    }
}
